import unittest
import app

class TestApp(unittest.TestCase):
    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()

    def test_hello_world(self):
        response = self.app.get('/')
        self.assertEqual(response.data, b'Hello, World!')

if __name__ == '__main__':
    unittest.main()

